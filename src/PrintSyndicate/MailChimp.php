<?php
namespace PrintSyndicate;

class MailChimp extends APIResource
{
    public static function addToList($listId, $email){
        $response = self::post('mail/list/add', [
            'list'=>$listId,
            'email'=>$email
        ]);
        return $response;
    }
}