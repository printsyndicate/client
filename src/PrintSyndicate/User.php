<?php
namespace PrintSyndicate;

class User extends APIResource{


    public static function login($email, $password){

        $response = self::postAndStoreSession(PrintSyndicate::getCurrentSite().'/user/login', [
            'user'=>[
                'email'=>$email,
                '_password'=>$password
            ]
        ]);

        if(isset($response['error'])){
            return $response;
        }

        self::setCookiesOnSuccessfulLogin($response);
        return $response;
    }

    public static function getOrders(){
        $orders = self::get(PrintSyndicate::getCurrentSite().'/user/orders/history');
        return $orders;
    }

    public static function getProfile(){
        $profile = self::get(\PrintSyndicate::getCurrentSite().'/user/info');
        return $profile;
    }

    public static function getEmail(){
        //Should we fetch the profile, if they're not logged in or have no email we do not
        if(!self::isAuthenticated() || (isset($_COOKIE['hasNoEmail']) && $_COOKIE['hasNoEmail'] == true))
            return null;

        if(isset($_COOKIE['authenticatedEmail']))
            return $_COOKIE['authenticatedEmail'];

        //Get the profile, if the profile has no email associated with it, store that so we don't keep calling the API
        $profile = self::getProfile();
        if(!isset($profile['email']))
            return parent::setCookie('hasNoEmail', true);

        parent::setCookie('authenticatedEmail', $profile['email']);
        return $profile['email'];
    }

    public static function postProfile($firstName, $lastName, $street1, $street2, $city, $state, $zip, $phone, $countryCode, $company, $birthday){
        $profile = self::post(\PrintSyndicate::getCurrentSite().'/user/info', [
            'firstName'=>$firstName,
            'lastName'=>$lastName,
            'street1'=>$street1,
            'street2'=>$street2,
            'city'=>$city,
            'state'=>$state,
            'zip'=>$zip,
            'phone'=>$phone,
            'countryCode'=>$countryCode,
            'company'=>$company,
            'birthday'=>$birthday
        ]);

        return $profile;
    }


    public static function register($email, $password, $mergeCart = null){
        $newUser = self::post(PrintSyndicate::getCurrentSite().'/user/register', [
            'user'=>[
                'email'=>$email,
                '_password'=>$password
            ],
            'mergeCart'=>$mergeCart
        ]);
        return $newUser;
    }

    public static function logout(){

        $response = self::get(PrintSyndicate::getCurrentSite().'/user/logout');
        if(!isset($response['error'])){
            self::destroyCookie('token');
            self::destroyCookie('authenticatedEmail');
            self::destroyCookie('isAuthenticated');
            self::destroyCookie('rememberMeToken');
            self::destroyCookie("PHPSESSID");
            self::destroyCookie('itemsInCart');
        }
        parent::$isAuthenticated = false;

    }

    public static function getShippingAddresses($page = 1, $perPage = 10){
        $response = self::get(\PrintSyndicate::getCurrentSite().'/user/shipping');
        return $response;
    }


    public static function newAddress($firstName, $lastName, $email, $street1, $street2, $city, $state, $zip, $phone, $countryCode, $nickname, $defaultFlag){
        $address = self::post(\PrintSyndicate::getCurrentSite().'/user/address/shipping', [
            'address'=>[
                'firstName'=>$firstName,
                'lastName'=>$lastName,
                'email'=>$email,
                'street1'=>$street1,
                'street2'=>$street2,
                'city'=>$city,
                'state'=>$state,
                'zip'=>$zip,
                'phone'=>$phone,
                'countryCode'=>$countryCode,
                'nickname'=>$nickname,
                'defaultFlag'=>$defaultFlag
            ]

        ]);
        return $address;
    }

    public static function updateAddress($guid, $firstName, $lastName, $email, $street1, $street2, $city, $state, $zip, $phone, $countryCode, $nickname, $defaultFlag){

        $address = self::post(\PrintSyndicate::getCurrentSite().'/user/address/shipping/'.$guid, [
            'address'=>[
                'firstName'=>$firstName,
                'lastName'=>$lastName,
                'email'=>$email,
                'street1'=>$street1,
                'street2'=>$street2,
                'city'=>$city,
                'state'=>$state,
                'zip'=>$zip,
                'phone'=>$phone,
                'countryCode'=>$countryCode,
                'nickname'=>$nickname,
                'defaultFlag'=>$defaultFlag
            ]

        ]);
        return $address;
    }

    public static function removeAddress($guid){
        $removal = parent::deleteWithToken(PrintSyndicate::getCurrentSite().'/user/address/remove/'.$guid);
        return $removal;
    }

    public static function getAddress($guid){
        return parent::get(PrintSyndicate::getCurrentSite().'/user/address/fetch/'.$guid);
    }


    public static function resetPasswordemail($email, $redirect){
        $resetPassword = self::post(PrintSyndicate::getCurrentSite().'/user/password/reset', [
            'email'=>$email,
            'redirect'=>$redirect
        ]);
        if(isset($resetPassword['error'])){
            return $resetPassword;
        }
        else {
            return $resetPassword;
        }

    }

    public static function checkresetToken($token){
        $checktoken = self::get(PrintSyndicate::getCurrentSite().'/user/password/reset/'.$token);
        if(isset($checktoken['error'])){
            return "Please reset the token again";
        }
        else {
            return $checktoken;
        }

    }

    public static function changePassword($token,$password){
        $changePassword = self::post(PrintSyndicate::getCurrentSite().'/user/password/reset/'.$token,[
            '_password'=>$password
        ]);
        if(isset($changePassword['error'])){
            return "Please reset the token again";
        }
        else {
            return $changePassword;
        }
    }

    public static function getTwitterAuthorizeURL($callback){
        return parent::post(\PrintSyndicate::getCurrentSite().'/user/twitter', ['callback'=>$callback]);
    }

    public static function loginWithTwitter($oauthToken, $oauthVerifier){
        $response = parent::postAndStoreSession(\PrintSyndicate::getCurrentSite().'/user/twitter/login', [
            'oauth_token'=>$oauthToken,
            'oauth_verifier'=>$oauthVerifier
        ]);
        self::setCookiesOnSuccessfulLogin($response);
    }

    public static function loginWithFacebook($facebookId){
        $response = parent::postAndStoreSession(\PrintSyndicate::getCurrentSite().'/user/facebook', [
            'facebookId'=>$facebookId
        ]);
        self::setCookiesOnSuccessfulLogin($response);
    }

    public static function loginWithToken($rememberMeToken){
        $response = parent::postAndStoreSession(\PrintSyndicate::getCurrentSite().'/user/tokenLogin', [
            'rememberToken'=>$rememberMeToken
        ]);
        if(isset($response['error'])){
            return $response;
        }
        self::setCookiesOnSuccessfulLogin($response);
        return $response;
    }

    /**
     * @param $response
     */
    protected static function setCookiesOnSuccessfulLogin($response)
    {
        $cookieExpiration = strtotime(date('Y-m-d H:i:s')) + parent::$cookieTimeout;
        parent::setCookie('isAuthenticated', true, $cookieExpiration);
        parent::setCookie('rememberMeToken', $response['result']['user']['rememberToken'], $cookieExpiration);
        parent::$isAuthenticated = true;

        if (PrintSyndicate::getCurrentSite() != 'syndicate') {
            self::setToken($response['result']['cartToken']);
            self::destroyCookie('itemsInCart');

            $cart = Cart::getCart($response['result']['cartToken']);
            if (isset($cart['items'])) {
                $count = 0;
                foreach ($cart['items'] as $item) {
                    $count += $item['quantity'];
                }
                self::setCookie('itemsInCart', $count);
            }
        }
    }

    public static function mergeToken($token){
        $cart=self::post(PrintSyndicate::getCurrentSite().'/user/token/merge',[
            'mergeToken'=>$token
        ]);
        return $cart;
    }
}
