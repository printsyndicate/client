<?php
namespace PrintSyndicate;

class PaymentMethods extends APIResource{

    public static function listMethods(){
        return parent::get(PrintSyndicate::getCurrentSite().'/paymentMethods/user/cards');
    }

    public static function getCustomer(){
        return parent::get(\PrintSyndicate::getCurrentSite().'/paymentMethods/customer');
    }

    public static function createCustomer($source, $cardName, $street1, $street2, $city, $state, $country, $zip){
        return parent::post(\PrintSyndicate::getCurrentSite().'/paymentMethods/create', [
            'source'=>$source,
            'cardName'=>$cardName,
            'billingAddress'=>[
                "city" => $city,
                "countryCode" => $country,
                "state" => $state,
                "street1" => $street1,
                "street2" => $street2,
                "zip" => $zip
            ]
        ]);
    }

    public static function addCard($source, $cardName, $street1, $street2, $city, $state, $country, $zip, $isDefault){
        return parent::post(PrintSyndicate::getCurrentSite().'/paymentMethods/add', [
            'cardName'=>$cardName,
            'source'=>$source,
            'isDefault'=>$isDefault,
            'billingAddress'=>[
                "city" => $city,
                "countryCode" => $country,
                "state" => $state,
                "street1" => $street1,
                "street2" => $street2,
                "zip" => $zip
            ]
        ]);
    }

    public static function updateCard($paymentMethod, $expirationMonth, $expirationYear, $cardName, $street1, $street2, $city, $state, $country, $zip, $isDefault){
        return parent::post(PrintSyndicate::getCurrentSite().'/paymentMethods/update', [
            'cardName'=>$cardName,
            'paymentMethod'=>$paymentMethod,
            'isDefault'=>$isDefault,
            'expiration_month'=>$expirationMonth,
            'expiration_year'=>$expirationYear,
            'billingAddress'=>[
                "city" => $city,
                "countryCode" => $country,
                "state" => $state,
                "street1" => $street1,
                "street2" => $street2,
                "zip" => $zip
            ]
        ]);
    }

    public static function deleteMethod($guid){
        return parent::post(PrintSyndicate::getCurrentSite().'/paymentMethods/delete', [
            'paymentMethod'=>$guid
        ]);
    }
}