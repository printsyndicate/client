<?php
namespace PrintSyndicate;

class Browse extends APIResource
{
    private static $perPageInGrid = 24;
    private static $pageTitle;
    private static $tags;
    private static $types;
    private static $filters = [];

    private static function setTags($response){
        self::$tags = [];
        if(isset($response['facets']['tags'])){
            self::$tags = $response['facets']['tags']['buckets'];
        }

    }

    private static function addFilter($filterName, $filterKey){
        self::$filters[$filterKey] = Keyword::get($filterName);
    }

    private static function removeFilter($filterKey){
        unset(self::$filters[$filterKey]);
    }

    public static function getFilters(){
        return self::$filters;
    }

    /**
     * @return mixed
     */
    public static function getTypes()
    {
        return self::$types;
    }

    public static function setTypes($response){
        self::$types = [];
        if(isset($response['facets']['product_type'])){
            self::$types = $response['facets']['product_type']['buckets'];
        }
    }

    private static function addFiltersToQuery($query, $type, $sort, $category, $tag){
        if($type != null){
            self::addFilter($type, 'type');
            $query.='&filters[]=product_type|'.$type;
        }

        if($tag != null){
            self::addFilter($tag, 'tag');
            $query.='&filters[]=tags|'.urlencode($tag);
        }

        if($sort != null){
            self::addFilter($sort, 'sort');
            $query.='&sort='.$sort;
        }
        if($category != null){
            self::addFilter($category, 'category');
            $query.='&filters[]=category|'.$category;
        }
        return $query;
    }


    public static function home(){
        $response = self::get(PrintSyndicate::$currentSite.'/page/home?page=1&perPage=10&fromDb=true');
//        self::validateJSONAgainstSchema('home.json', '/page/home');
        return $response;
    }

    public static function setCommonBrowseData($response, $page){
        self::setTags($response);
        self::setTypes($response);
        Pagination::setPaginationDataForResponse($response, $page, self::$perPageInGrid);
    }

    public static function type($type, $page = 1, $sort = null, $category = null, $tag = null){
        self::setPageTitle($type);
        $baseQuery = PrintSyndicate::$currentSite . '/products/list?type=' . $type . '&page=' . $page . '&perPage=' . self::$perPageInGrid;
        $query = self::addFiltersToQuery($baseQuery, null, $sort, $category, $tag);
        $response = self::get($query);
        self::validateJSONAgainstSchema('amazon.products.json', $query);
        self::setCommonBrowseData($response, $page);
        return $response;
    }



    public static function tag($type, $page = 1, $sort = null, $category = null, $tag = null){
        $baseQuery = PrintSyndicate::$currentSite . '/products/list?page=' . $page . '&perPage=' . self::$perPageInGrid;
        $query = self::addFiltersToQuery($baseQuery, $type, $sort, $category, $tag);
        $response = self::get($query);
        if(isset($response['redirect_id']))
            return $response;
        self::setCommonBrowseData($response, $page);
        self::removeFilter('tag');
        return $response;

    }


    public static function pill($pill, $page = 1, $type = null, $sort = null, $category = null, $tag = null, $productType = null){

        self::setPageTitle($pill);
        if($pill == 'bestsellers'){
            $pill = 'best';
        }
        if($pill == 'trending'){
            $pill = 'hot';
        }
        $baseQuery = PrintSyndicate::$currentSite . '/products/list/' . $pill . '?page=' . $page . '&perPage=' . self::$perPageInGrid;

        $query = self::addFiltersToQuery($baseQuery, $type, $sort, $category, $tag);
        if($productType != null){
            $query .= '&productType='.$productType;
        }
        $response = self::get($query);
        self::validateJSONAgainstSchema('amazon.products.json', $query);
        self::setCommonBrowseData($response, $page);
        return $response;
    }



    public static function notFoundRelated($productId){
        return self::get(\PrintSyndicate::getCurrentSite().'/product/'.$productId.'/topics/related');
    }

    public static function category($categoryName, $page = 1, $type = null, $sort = null, $category = null, $tag = null){
        self::setPageTitle($categoryName);
        $baseQuery = PrintSyndicate::$currentSite . '/products/list?category=' . $categoryName . '&page=' . $page . '&perPage=' . self::$perPageInGrid;
        $query = self::addFiltersToQuery($baseQuery, $type, $sort, $category, $tag);
        $response = self::get($query);
        self::validateJSONAgainstSchema('amazon.products.json', $query);
        self::setCommonBrowseData($response, $page);
        return $response;
    }

    public static function collection($id, $page = 1, $type = null, $sort = null, $category = null, $tag = null, $perPage = null){
        if($perPage === null){
            $perPage = self::$perPageInGrid;
        }

        $baseQuery = PrintSyndicate::$currentSite . '/collection/'.$id.'/products?fromDb=true&page='.$page.'&perPage='.$perPage;
        $query = self::addFiltersToQuery($baseQuery, $type, $sort, $category, $tag);
        $response = self::get($query);
        self::validateJSONAgainstSchema('browse.collection.json', $query);
        self::setPageTitleExplicit($response['name']);
        $productData = $response['products'];
        self::setCommonBrowseData($productData, $page);
        return $response;
    }

    public static function search($query, $page = 1, $type = null, $sort = null, $category = null, $tag = null){
        $searchQuery = $query;
        $baseQuery = PrintSyndicate::$currentSite . '/products/search/' . str_replace('-', '+', $query) . '?page=' . $page . '&perPage=' . self::$perPageInGrid;
        $query = self::addFiltersToQuery($baseQuery, $type, $sort, $category, $tag);
        $response = self::get($query);
        self::validateJSONAgainstSchema('amazon.products.json', $query);
        $collectionsQuery = PrintSyndicate::$currentSite.'/collections/search/'.$searchQuery.'?page=1&perPage=4';
        $collectionsResponse = self::get($collectionsQuery);
        if (count($collectionsResponse['hits']['hit']) > 0)
            $response['collections'] = $collectionsResponse['hits']['hit'];
        self::setPageTitleExplicit('Results for '.ucwords(str_replace('-', ' ', $searchQuery)));
        self::setCommonBrowseData($response, $page);
        return $response;
    }

    public static function collections($page = 1){
        $response = self::get(PrintSyndicate::getCurrentSite().'/page/collections?page='.$page.'&perPage='.self::$perPageInGrid);
        self::validateJSONAgainstSchema('amazon.collections.json', '/page/collections');
        self::setCommonBrowseData($response['collections'], $page);
        return $response;
    }

    /**
     * @return mixed
     */
    private static function setPageTitle($keyword)
    {

        self::$pageTitle = Keyword::get($keyword);
    }

    private static function setPageTitleExplicit($newTitle){
        self::$pageTitle = $newTitle;
    }

    /**
     * @return mixed
     */
    public static function getPageTitle()
    {
        return self::$pageTitle;
    }

    /**
     * @return mixed
     */
    public static function getTags()
    {
        return self::$tags;
    }
}