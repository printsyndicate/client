<?php
namespace PrintSyndicate;

use JsonSchema\Uri\UriRetriever;

class RenderConfig extends APIResource{

    public function getRenderConfigs(){
        $retriever = new UriRetriever();
        $renders = $retriever->retrieve('file://' . __DIR__.'/JSONSchemas/config.renders.json');
        return $renders;
    }
}
?>