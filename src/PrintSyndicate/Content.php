<?php
namespace PrintSyndicate;

class Content extends APIResource
{
    public static function headerPromotion(){
        $response = self::get(PrintSyndicate::$currentSite.'/site/header-product-promotion-message/content', true);
        return $response;
    }
    public static function headerShipping(){
        $response = self::get(PrintSyndicate::$currentSite.'/site/header-shipping-promotion-message/content', true);
        return $response;
    }
    public static function cartShippingPromotionMessage() {
        $response = self::get(PrintSyndicate::$currentSite.'/site/cart-shipping-promotion-message/content', true);
        return $response;
    }

    public static function getReferenceContent($reference_name) {
        $response = self::get(PrintSyndicate::$currentSite.'/site/'.$reference_name.'/content', true);
        return $response;
    }

    public static function getContentText($reference_name){
        $response = self::get(PrintSyndicate::$currentSite.'/site/'.$reference_name.'/content', true);

        return isset($response['html']) ? $response['html'] : "";
    }
}