<?php
namespace PrintSyndicate;

class Keyword
{

    public static function get($keyword)
    {
        $mappings = [
            'new' => 'New',
            'accessories' => 'Accessories',
            'bestsellers' => 'Best Sellers',
            'sale' => 'Sale',
            'trending' => 'Trending',
            'homedecor' => 'Home Decor',
            'clothing' => 'Clothing',
            'phonecase'=>'Phone Case',
            'tabletcase'=>'Tablet Case',
            'pillow'=>'Throw Pillow',
            'tote'=>'Tote Bag',
            'blanket'=>'Blanket',
            'canvas'=>'Canvas Print',
            'poster'=>'Poster',
            'mug'=>'Coffee Mug',
            'giftcard'=>'Gift card',
            'greetingcard'=>'Greeting Card',
            'sticker'=>'Sticker',
            'towel'=>'Towel',
            'tattoo'=>'Temporary Tattoo',
            'accessorybag'=>'Accessory Bag',
            'wall-decal'=>'Wall Decal',
            'legging'=>'Women\'s Leggings',
            'baby'=>'Baby One-Piece',
            'tee'=>'T-Shirt',
            'vneck'=>'V-Neck Tee',
            'tank'=>'Tank Top',
            'baseball'=>'Baseball Tee',
            'hoodie'=>'Hoodie',
            'racerback'=>'Racerback Tank',
            'pullover'=>'Crewneck Sweatshirt',
            'sock'=>'Socks',
            'oldest' => 'Oldest First',
            'newest' => 'Newest First',
            'towel'=>'Beach Towels',
            'flexicase' => 'Phone Flexi-Cases',
            'title_asc' => 'Alphabetical',
            'title_desc' => 'Reverse Alphabetical',
            'popular' => 'Most Popular First',
            "/browse/tee"=>'tees',
            "/browse/racerback"=>'racerback-tanks',
            "/browse/tank"=>'tank-tops',
            "/browse/vneck"=>'v-neck-tees',
            "/browse/baseball"=>'baseball-tees',
            "/browse/pullover"=>'lightweight-pullovers',
            "/browse/hoodie"=>'hoodies',
            "/browse/baby"=>'baby-one-piece'
        ];
        if(!isset($mappings[$keyword])){
            return $keyword;
        }
        return $mappings[$keyword];
    }


    public static function getUri($key){
        $mappings = [
            'phone-case'=>'phonecase',
            'tablet-case'=>'tabletcase',
            'throw-pillow'=>'pillow',
            'tote-bag'=>'tote',
            'blanket'=>'blanket',
            'canvas-print'=>'canvas',
            'poster'=>'poster',
            'mug'=>'mug',
            'gift-cards'=>'giftcard',
            'greetingcard'=>'greetingcard',
            'sticker'=>'sticker',
            'towel'=>'towel',
            'temporary-tattoo'=>'tattoo',
            'accessory-bag'=>'accessorybag',
            'wall-decal'=>'wall-decal',
            'leggings'=>'legging',
            'onesie'=>'baby',
            'tshirt'=>'tee',
            'vneck'=>'vneck',
            'tank-top'=>'tank',
            'baseball-shirt'=>'baseball',
            'hoodie'=>'hoodie',
            'racerback-tank'=>'racerback',
            'pullovers'=>'pullover'
        ];
        if(!isset($mappings[$key])){
            return $key;
        }
        return $mappings[$key];

    }

    public static function humanReadable($key){
        $mappings = [
            'tshirt'=>'t-shirt',
        ];


        if(!isset($mappings[$key]))
            return $key;

        return $mappings[$key];
    }

    public static function getForTitle($key){
        $mappings = [
            'phonecase'=>'Phone Case',
            'tabletcase'=>'Tablet Case',
            'pillow'=>'Throw Pillow',
            'tote'=>'Tote Bag',
            'blanket'=>'Blanket',
            'canvas'=>'Canvas Print',
            'poster'=>'Poster',
            'mug'=>'Coffee Mug',
            'giftcard'=>'Gift card',
            'greetingcard'=>'Greeting Card',
            'sticker'=>'Sticker',
            'towel'=>'Towel',
            'tattoo'=>'Temporary Tattoo',
            'accessorybag'=>'Accessory Bag',
            'wall-decal'=>'Wall Decal',
            'legging'=>'Women\'s Leggings',
            'baby'=>'Onesie',
            'tee'=>'T-Shirt',
            'vneck'=>'V-Neck Tee',
            'tank'=>'Tank Top',
            'baseball'=>'Baseball Tee',
            'hoodie'=>'Hoodie',
            'racerback'=>'Racerback Tank',
            'pullover'=>'Crewneck Sweatshirt',
            'sock'=>'Socks',
            'flexicase' => 'Phone Flexi-Case'
        ];

        if(!isset($mappings[$key])){
            return $key;
        }
        return $mappings[$key];
    }

    public static function getReverseUri($key){
        $mappings = [
            'phonecase'=>'phone-case',
            'tabletcase'=>'tablet-case',
            'pillow'=>'throw-pillow',
            'tote'=>'tote-bag',
            'blanket'=>'blanket',
            'canvas'=>'canvas-print',
            'poster'=>'poster',
            'mug'=>'mug',
            'giftcard'=>'gift-cards',
            'greetingcard'=>'greetingcard',
            'sticker'=>'sticker',
            'towel'=>'towel',
            'tattoo'=>'temporary-tattoo',
            'accessorybag'=>'accessory-bag',
            'wall-decal'=>'wall-decal',
            'legging'=>'leggings',
            'baby'=>'onesie',
            'tee'=>'tshirt',
            'vneck'=>'vneck',
            'tank'=>'tank-top',
            'baseball'=>'baseball-shirt',
            'hoodie'=>'hoodie',
            'racerback'=>'racerback-tank',
            'pullover'=>'pullovers'
        ];
        if(!isset($mappings[$key])){
            return $key;
        }
        return $mappings[$key];
    }
}