<?php
namespace PrintSyndicate;

class MagicTags extends APIResource {
    public static function getMagicTagResponses(){
        $responses = parent::$getResultCache;
        $magicTags = [];
        foreach($responses as $response){
            if(isset($response['magicTags']))
                $magicTags[] = $response['magicTags'];
        }
        $responses = parent::$postResultsCache;
        foreach($responses as $response){
            if(isset($response['magicTags']))
                $magicTags[] = $response['magicTags'];
        }
        return $magicTags;
    }

    public static function hasMagicTags(){
        return count(self::getMagicTagResponses()) > 0;
    }

    public static function getMagicTags($tags, $event = null){
        if(!is_array($tags))
            $tags = [$tags];
        return parent::post(PrintSyndicate::getCurrentSite().'/tags/magic_tags', [
            'tags'=>$tags,
            'event'=>$event
        ]);
    }
}