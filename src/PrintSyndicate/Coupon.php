<?php
namespace PrintSyndicate;

class Coupon extends APIResource
{
    public static function addToCart($code){
        $respnose = self::postWithToken(PrintSyndicate::getCurrentSite().'/cart/coupon', [
            'code'=>$code
        ]);

        return $respnose;
    }
}