<?php
namespace PrintSyndicate;

use PrintSyndicate\Errors\DefaultErrorReporter;

class PrintSyndicate{

    public static $baseURL;
    public static $currentSite;
    public static $enableErrorReporting = true;

    /**
     * @return boolean
     */
    public static function isEnableErrorReporting()
    {
        return self::$enableErrorReporting;
    }

    /**
     * @param boolean $enableErrorReporting
     */
    public static function setEnableErrorReporting($enableErrorReporting)
    {
        self::$enableErrorReporting = $enableErrorReporting;
    }
    public static $errorClass = null;

    /**
     * @return mixed
     */
    public static function getBaseURL()
    {
        $allowedHosts = [
            'lookhuman.com',
            'www.lookhuman.com',
            'mericamade.com',
            'www.mericamade.com',
            'activateapparel.com',
            'www.activateapparel.com'
        ];
        if(self::$baseURL == null && isset($_SERVER['SERVER_NAME']) && in_array(strtolower($_SERVER['SERVER_NAME']), $allowedHosts)){
            self::$baseURL = 'api.printsyndicate.com';
        }
        return self::$baseURL;
    }


    public static function getErrorReporter(){
        if(self::$errorClass == null){
            return DefaultErrorReporter::class;
        }
        return self::$errorClass;
    }

    public static function setErrorReporter($errorClass){
        self::$errorClass = $errorClass;
    }

    /**
     * @param mixed $baseURL
     */
    public static function setBaseURL($baseURL)
    {
        self::$baseURL = $baseURL;
    }

    /**
     * @return mixed
     */
    public static function getCurrentSite()
    {
        return self::$currentSite;
    }

    /**
     * @param mixed $currentSite
     */
    public static function setCurrentSite($currentSite)
    {
        self::$currentSite = $currentSite;
    }
}