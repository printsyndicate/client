<?php
namespace PrintSyndicate;

class RecentlyViewed extends APIResource
{
    public static function getProducts(){
        $response = parent::getWithToken(PrintSyndicate::getCurrentSite().'/products/recent');
        return $response;
    }

    public static function addProduct($id, $ref = null){
        $response = parent::postWithToken(PrintSyndicate::getCurrentSite().'/product/'.$id.'/viewed?ref='.$ref);
        return $response;
    }
}