<?php
namespace PrintSyndicate;

class Pages extends APIResource
{
    public static function getModules(){
        $requestURI = \Request::getRequestUri();
        $requestURI = explode('?', $requestURI);
        $requestURI = $requestURI[0];
        $requestURI = urlencode($requestURI);
        $timeWarpString = '';
        if(\Request::get('timeWarp') != null)
            $timeWarpString = '&timeWarp=' . \Request::get('timeWarp');

        $url = PrintSyndicate::getCurrentSite() . '/pages/url?url=' . $requestURI . $timeWarpString;
        return parent::get($url);
    }

    public static function getForURL($url){
        $timeWarpString = '';
        if(\Request::get('timeWarp') != null)
            $timeWarpString = '&timeWarp=' . \Request::get('timeWarp');

        $url = PrintSyndicate::getCurrentSite() . '/pages/url?url=' . $url . $timeWarpString;
        return parent::get($url);
    }
}