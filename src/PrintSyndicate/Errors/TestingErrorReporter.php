<?php
namespace PrintSyndicate\Errors;

class TestingErrorReporter implements ErrorReporter{

    public static function handle($response)
    {
        if(!isset($response['error'])){
            return;
        }
        throw new \Exception($response['error']['message']);
    }
}