<?php
namespace PrintSyndicate\Errors;

interface ErrorReporter{
    public static function handle($response);
}