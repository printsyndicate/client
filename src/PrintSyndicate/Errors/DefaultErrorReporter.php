<?php
namespace PrintSyndicate\Errors;

class DefaultErrorReporter implements ErrorReporter{

    public static function handle($response)
    {
        if(!isset($response['error'])){
            return;
        }
        http_response_code($response['error']['http_code']);
        echo $response['error']['message'];
        exit;
    }
}