<?php
namespace PrintSyndicate;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\ClientException;

class Pagination{
    public static $results;
    public static $page;
    public static $pages;

    public static function setPaginationDataForResponse($response, $page, $perPage){
        self::setPage($page);
        self::setPages(ceil($response['hits']['found'] / $perPage));
        self::setResults($response['hits']['found']);
    }


    public static function getNextPageHref(){
        if(self::getPages() == self::$page){
            return '#';
        }
        $nextPageNumber = (int)self::$page + 1;

        return self::updatePageNumber($nextPageNumber);
    }
    public static function getPreviousPageHref(){
        if(self::$page == 1){
            return '#';
        } else if(self::$page == 2){
            $urlArray = explode('?', self::getBaseUri());
            $url = $urlArray[0];
            if(isset($urlArray[1])){
                $params = explode('&', $urlArray[1]);
                foreach($params as $index=>$param){
                    $paramArray = explode('=', $param);
                    if($paramArray[0] == 'page'){
                        unset($params[$index]);
                    }
                }
                $params = implode('&', $params);
                $url = $urlArray[0].'?'.$params;
            }

            return $url;
        }

        return self::updatePageNumber(((int)self::$page)-1);
    }

    /**
     * @return mixed
     */
    public static function getResults()
    {
        return self::$results;
    }

    /**
     * @param mixed $results
     */
    public static function setResults($results)
    {
        self::$results = $results;
    }

    /**
     * @return mixed
     */
    public static function getPage()
    {
        return self::$page;
    }

    /**
     * @param mixed $page
     */
    public static function setPage($page)
    {
        self::$page = $page;
    }

    /**
     * @return mixed
     */
    public static function getPages()
    {
        return self::$pages;
    }

    /**
     * @param mixed $pages
     */
    public static function setPages($pages)
    {
        self::$pages = $pages;
    }

    /**
     * @return array|string
     */
    protected static function getBaseUri()
    {
        $baseURI = $_SERVER['REQUEST_URI'];
        return $baseURI;
    }

    /**
     * @param $newPageNumber
     * @return string
     */
    protected static function updatePageNumber($newPageNumber)
    {
        $baseURI = self::getBaseUri();
        $baseURIArray = explode('?', $baseURI);
        $pageParamFound = false;
        if (!isset($baseURIArray[1])) {
            $url = $baseURI . '?page=' . $newPageNumber;
        } else {
            $params = explode('&', $baseURIArray[1]);
            foreach ($params as $index => $param) {
                $paramArray = explode('=', $param);
                if ($paramArray[0] == 'page') {

                    $paramArray[1] = ($newPageNumber);
                    $params[$index] = $paramArray[0] . '=' . $paramArray[1];
                    $pageParamFound = true;
                }
            }
            if(!$pageParamFound){

                $params[]='page=2';
            }

            $params = implode('&', $params);
            $url = $baseURIArray[0] . '?' . $params;

        }
        return $url;
    }

}