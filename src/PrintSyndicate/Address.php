<?php
namespace PrintSyndicate;

class Address extends APIResource
{
    public static function validate($street1, $street2, $city, $state, $zip, $country, $cartId){

        $response = self::post('shipment/validateAddress', [
            'cart'=>$cartId,
            'address'=>[
                'city'=>$city,
                'countryCode'=>$country,
                'street1'=>$street1,
                'street2'=>$street2,
                'state'=>$state,
                'zip'=>$zip
            ]
        ]);


        return $response;
    }

}