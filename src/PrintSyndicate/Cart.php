<?php
namespace PrintSyndicate;

class Cart extends APIResource
{


    public static function add($sku, $quantity = 1, $recipientEmail = null, $recipientMessage = null)
    {

        $data = [
            'sku' => $sku,
            'quantity' => $quantity,
            'email' => $recipientEmail,
            'message' => $recipientMessage
        ];

        $response = parent::postWithToken(PrintSyndicate::getCurrentSite() . '/cart', $data);
        self::validateJSONAgainstSchema('cart.json', '/cart/add');

        if (isset($response['error'])) {

        }



        self::updateItemCount($response);
        return $response;
    }

    public static function removeGiftCard($giftCardId){
        $response = parent::deleteWithToken(PrintSyndicate::getCurrentSite().'/cart/giftcard/'.$giftCardId);

        return $response;
    }


    public static function getFlyoutData()
    {
        $cartData = parent::get(PrintSyndicate::getCurrentSite() . '/cart/' . parent::getToken());
        self::validateJSONAgainstSchema('cart.json', 'Flyout Data');
        $maxDate = 0;
        $maxItem = null;
        $count = 0;
        foreach ($cartData['items'] as $item) {
            $count += $item['quantity'];
            if (strtotime($item['update_date']) > $maxDate) {
                $maxDate = strtotime($item['update_date']);
                $maxItem = $item;
            }
        }

        $blankSku = explode('-', $maxItem['sku']);
        array_shift($blankSku);
        $blankSku = implode('-', $blankSku);

        $response = [
            'latestItem' => $maxItem,
            'subTotal' => $cartData['subTotal'],
            'numItems' => $count,
            'blankSku' => $blankSku,
            'cart' => $cartData
        ];
        return $response;
    }


    public static function removeItem($itemId)
    {
        $response = self::postWithToken(PrintSyndicate::getCurrentSite() . '/item/' . $itemId . '/remove');
        self::validateJSONAgainstSchema('cart.json', '/cart/remove');
        if (!isset($response['error'])) {
            self::updateItemCount($response);
        }

        return $response;
    }


    public static function getCart($token = null)
    {
        if ($token == null) {
            $token = parent::getToken();
        }
        $response = parent::get(PrintSyndicate::getCurrentSite() . '/cart/' . $token);
        self::validateJSONAgainstSchema('cart.json', '/cart/get');
        self::updateItemCount($response);
        return $response;
    }


    public static function updateItem($itemId, $newSku = null, $quantity = null)
    {
        $data = [];
        if ($newSku != null) {
            $data['sku'] = $newSku;
        }
        if ($quantity != null) {
            $data['quantity'] = $quantity;
        }

        $response = parent::postWithToken(PrintSyndicate::getCurrentSite() . '/item/' . $itemId, $data);
        self::validateJSONAgainstSchema('cart.json', '/cart/update/item');
        self::updateItemCount($response);

        return $response;
    }

    /**
     * @param $response
     */
    public static function updateItemCount($response = null)
    {
        $count = 0;
        if($response != null && isset($response['items'])){
            foreach ($response['items'] as $item) {
                $count += $item['quantity'];
            }
        }

        parent::destroyCookie('itemsInCart');

        parent::setCookie('itemsInCart', $count);
    }

    public static function lookupOrder($shippingAddressEmail, $cartId = null)
    {
        $orderLookupData = parent::post(PrintSyndicate::getCurrentSite() . '/customer/cart/lookup', ['shippingAddressEmail' => $shippingAddressEmail, 'cartId' => $cartId]);
        return $orderLookupData;
    }

    public static function receipt($guid) {
        $receiptData = parent::get(PrintSyndicate::getCurrentSite() . '/receipt/' . $guid);
        self::validateJSONAgainstSchema('cart.json', '/cart/receipt');
        return $receiptData;
    }

    public static function updateShipmentItem($itemGuid, $color, $size) {
        $data = [ 'color' => $color, 'size' => $size ];
        $response = parent::post(PrintSyndicate::getCurrentSite().'/customer/cart/item/'.$itemGuid.'/update', $data);
        return $response;
    }

    public static function addShippingAddress($cartId, $street1, $street2, $city, $state, $country, $zip, $email, $phone, $firstName, $lastName, $validationUsed)
    {
        $addressAddResponse = parent::postWithToken(PrintSyndicate::getCurrentSite() . '/cart/' . $cartId . '/shipping', [
            'shippingAddress' => [
                "city" => $city,
                "email" => $email,
                "countryCode" => $country,
                "firstName" => $firstName,
                "lastName" => $lastName,
                "phone" => $phone,
                "state" => $state,
                "street1" => $street1,
                "street2" => $street2,
                "zip" => $zip,
                "validationUsed" => $validationUsed
            ]
        ]);
        self::validateJSONAgainstSchema('cart.json', '/cart/shipping/add');
        return $addressAddResponse;
    }

    public static function getShippingMethods($locale = null)
    {
        if($locale == null) {
            $data = parent::postWithToken('shipment/options');
        }

        if($locale != null){
            $data = parent::postWithToken('shipment/locale/options/'.$locale);
        }

        if (isset($data['error'])) {

        }

        return $data;
    }



    public static function updateShippingAddress ($cartGuid, $street1, $street2 = null, $city, $state, $zip, $firstName, $lastName, $validationUsed) {
        $response = parent::postWithToken(PrintSyndicate::getCurrentSite() . '/customer/cart/shipping', [
            'shippingAddress' => [
                "city" => $city,
                "firstName" => $firstName,
                "lastName" => $lastName,
                "state" => $state,
                "street1" => $street1,
                "street2" => $street2,
                "zip" => $zip,
                "validationUsed" => $validationUsed
            ],
            'guid' => $cartGuid
        ]);
        self::validateJSONAgainstSchema('cart.json', '/cart/shipping/update');
        return $response;
    }


    public static function setShippingMethods($methods, $cartId = null){
        if($cartId != null){
            $endpoint = 'shipment/set/' . $cartId;
        } else{
            $endpoint = 'shipment/set';
        }

        $response = parent::postWithToken($endpoint, $methods);

        if(isset($response['error'])){

        }

        return $response;
    }

    public static function checkout($source, $currency, $platform, $userAgent, $street1, $street2, $zip, $city, $state, $countryCode, $firstName, $lastName, $email, $phone){
        $response = parent::postWithToken(PrintSyndicate::getCurrentSite().'/cart/checkout', [
            'source'=>$source,
            'currency'=>$currency,
            'platform'=>$platform,
            'userAgent'=>$userAgent,
            'billingAddress'=>[
                'city'=>$city,
                'countryCode'=>$countryCode,
                'firstName'=>$firstName,
                'lastName'=>$lastName,
                'state'=>$state,
                'email'=>$email,
                'phone'=>$phone,
                'street1'=>$street1,
                'street2'=>$street2,
                'zip'=>$zip
            ]
        ]);
        self::validateJSONAgainstSchema('cart.json', '/cart/checkout');
        self::updateItemCount();
        return $response;
    }

    public static function removeCoupon($code){
        $response = parent::deleteWithToken(PrintSyndicate::getCurrentSite().'/cart/coupon', [
            'code'=>$code
        ]);

        return $response;
    }

    public static function setShippingAddressToStoredAddress($guid){
        return parent::get(\PrintSyndicate::getCurrentSite().'/cart/shipping/set/'.$guid);
    }

}