<?php
namespace PrintSyndicate;

class Wishlist extends APIResource{

    public static function getWishlists(){
        return parent::get(\PrintSyndicate::getCurrentSite().'/wishlist/get');
    }

    public static function addItem($sku, $wishlistId = null){
        return parent::post(\PrintSyndicate::getCurrentSite().'/wishlist/add', [
            'wishlist'=>$wishlistId,
            'item'=>[
                'sku'=>$sku
            ]
        ]);
    }

    public static function getWishlist($guid){
        return parent::get(\PrintSyndicate::getCurrentSite().'/wishlist/get/public/'.$guid);
    }

    public static function deleteItem($itemId){
        return parent::post(\PrintSyndicate::getCurrentSite().'/wishlist/item/delete', ['wishlistItem'=>$itemId]);
    }

    public static function addToCart($guid){
        return parent::postWithToken(\PrintSyndicate::getCurrentSite().'/wishlist/cart', [
            'wishlist'=>$guid
        ]);
    }

    public static function create($name){
        return parent::post(\PrintSyndicate::getCurrentSite().'/wishlist/create', ['name'=>$name]);
    }

    public static function update($name, $guid){
        return parent::post(\PrintSyndicate::getCurrentSite().'/wishlist/update', ['name'=>$name, 'wishlist'=>$guid]);
    }

    public static function delete($guid){
        return parent::deleteWithToken(\PrintSyndicate::getCurrentSite().'/wishlist/delete', [
            'wishlist'=>$guid
        ]);
    }
}