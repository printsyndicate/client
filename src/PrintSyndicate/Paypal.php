<?php
namespace PrintSyndicate;

class Paypal extends APIResource{

    public static function getPaypalURL($returnURL, $cancelURL){
        return self::postWithToken(PrintSyndicate::getCurrentSite().'/paypal/url', [
            'returnURL'=>$returnURL,
            'cancelURL'=>$cancelURL
        ]);
    }

    public static function getPaypalDetails($ppToken){
        return self::postWithToken(PrintSyndicate::getCurrentSite().'/paypal/details', [
            'ppToken'=>$ppToken
        ]);
    }

    public static function checkout($ppToken, $currency, $payerId){
        $response = self::postWithToken(PrintSyndicate::getCurrentSite().'/paypal/transact', [
            'ppToken'=>$ppToken,
            'currency'=>$currency,
            'payerId'=>$payerId
        ]);
        if(!isset($response['error'])){
            self::updateItemCount();
        }
        return $response;
    }

    /**
     * @param $response
     */
    protected static function updateItemCount($response = null)
    {
        $count = 0;
        if($response != null){
            foreach ($response['items'] as $item) {
                $count += $item['quantity'];
            }
        }

        parent::destroyCookie('itemsInCart');
        parent::setCookie('itemsInCart', $count);
    }

}