<?php
namespace PrintSyndicate;

class Collection extends APIResource
{

    public static function verify($id){
        $response = parent::get(PrintSyndicate::getCurrentSite().'/collection/'.$id.'/verify');
        return $response;
    }

}