<?php
namespace PrintSyndicate;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Event\CompleteEvent;
use JsonSchema\Uri\UriRetriever;
use JsonSchema\Validator;
use Satooon\JsonSchemaValidator\JsonSchemaValidator;

class APIResource{

    private static $client;
    protected static $getResultCache = [];
    protected static $postResultsCache = [];
    protected static $cookieTimeout = 604800;
    protected static $isAuthenticated;
    protected static $sessID;
    private static $jsonStringResponse;
    private static $token = '';

    public static function setClient()
    {
        if(PrintSyndicate::getBaseURL() == null){
            throw new \Exception('Base URL not set.  You must set the API base url before using the API client.');
        }

        $client = new Client([
            'base_url'=>'https://'.PrintSyndicate::getBaseURL().'/api/v1/',
            'defaults' => [
                'exceptions'=>false,
                'headers'=>[
                    'User-Agent'=>isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'FRONT-END-CLIENT',
                    'Referer'=>isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
                ]
            ]

        ]);
        if(strpos(PrintSyndicate::getBaseURL(), 'staging')){
            $client->getEmitter()->on('complete', function (CompleteEvent $e) {
                if($e->getResponse()->getStatusCode() == 506){
                    dd($e->getResponse()->getBody()->getContents());
                }
            });
        }
        self::$client = $client;

    }


    public static function getToken(){
        if(isset($_COOKIE['token'])){
            return $_COOKIE['token'];
        }

        if(self::$token != null){
            return self::$token;
        }

        if(self::isAuthenticated()){

        }

        $data = self::post(PrintSyndicate::getCurrentSite().'/token');
        self::setToken($data['token']);
        self::$token = $data['token'];
        self::destroyCookie('itemsInCart');
        self::destroyCookie('isAuthenticated');
        return $data['token'];
    }

    public static function setToken($token){
        self::destroyCookie('token');
        self::setCookie('token', $token);
    }

    public static function setCookie($name, $value, $time = null){
        if(getenv('APP_ENV') == 'testing'){
            return;
        }
        if($time == null){
            $time = time() + (10 * 365 * 24 * 60 * 60);
        }
        $serverHost = explode(':', $_SERVER['HTTP_HOST']);
        $serverHost = $serverHost[0];
        setcookie($name, $value, $time, '/');
    }

    /**
     * API information to return the token information of a user
     * @param $token
     * @return mixed
     */
    public static function getTokenInfo($token)
    {
        $response = self::get(PrintSyndicate::getCurrentSite() . '/user/token/' . $token);
        return $response;
    }

    /**
     * @return array
     */
    public static function getResponseStack()
    {
        return self::$getResultCache;
    }



    /**
     * @return int
     */
    public function getCookieTimeout()
    {
        return $this->cookieTimeout;
    }

    public static function get($url, $cacheable = true){

        self::setClient();

        if($cacheable && array_key_exists($url,self::$getResultCache))
            return self::$getResultCache[$url];

        $jar = false;

        $domain = PrintSyndicate::getBaseURL();
        $domain = explode('/', $domain)[0];
        if(isset($_COOKIE['PHPSESSID'])){
            $jar = new CookieJar();
            $cookie = new SetCookie();
            $cookie->setName('PHPSESSID');
            $cookie->setValue($_COOKIE['PHPSESSID']);
            $cookie->setDomain($domain);
            $jar->setCookie($cookie);
        }
        //On the initial request after logging in we need to use the stored session id because the cookie hasn't been added yet
        if(self::$sessID != null){
            $jar = new CookieJar();
            $cookie = new SetCookie();
            $cookie->setName('PHPSESSID');
            $cookie->setValue(self::$sessID);
            $cookie->setDomain($domain);
            $jar->setCookie($cookie);
        }

        try{
            $response = self::$client->get($url, ['cookies'=>$jar]);
        } catch(\Exception $e){
            $handler = PrintSyndicate::getErrorReporter();
            $handler = new $handler;
            $handler->handle([
                'error'=>[
                    'http_code'=>500,
                    'message'=>$e->getMessage()
                ]
            ]);
        }

        $jsonContents = $response->getBody()->getContents();
        self::$jsonStringResponse = $jsonContents;
        if($cacheable)
        {
            self::$getResultCache[$url] = self::transformJSONAndValidateResponse($jsonContents);
            return self::$getResultCache[$url];
        }
        return self::transformJSONAndValidateResponse($jsonContents);
    }


    public static function post($url, $data = null){
        self::setClient();
        $jar = new CookieJar();

        if(isset($_COOKIE['PHPSESSID'])){
            $cookie = new SetCookie();
            $cookie->setName('PHPSESSID');
            $cookie->setValue($_COOKIE['PHPSESSID']);
            $domain = PrintSyndicate::getBaseURL();
            $domain = explode('/', $domain)[0];
            $cookie->setDomain($domain);
            $jar->setCookie($cookie);
        }

        try{
            $response = self::$client->post($url, ['json'=>$data, 'cookies'=>$jar]);
        } catch(\Exception $e){
            $handler = PrintSyndicate::getErrorReporter();
            $handler = new $handler;
            $handler->handle([
                'error'=>[
                    'http_code'=>500,
                    'message'=>$e->getMessage()
                ]
            ]);
        }

        $jsonContents = $response->getBody()->getContents();
        self::$postResultsCache[$url] = self::transformJSONAndValidateResponse($jsonContents);
        return self::transformJSONAndValidateResponse($jsonContents);
    }

    private static function getClientIP(){

        $ip = "";

        if (!empty($_SERVER["HTTP_CLIENT_IP"]))
        {
            //check for ip from share internet
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            // Check for the Proxy User
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        else
        {
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    public static function postWithToken($url, $data = []){
        self::setClient();
        $jar = new CookieJar();

        if(isset($_COOKIE['PHPSESSID'])){
            $cookie = new SetCookie();
            $cookie->setName('PHPSESSID');
            $cookie->setValue($_COOKIE['PHPSESSID']);
            $domain = PrintSyndicate::getBaseURL();
            $domain = explode('/', $domain)[0];
            $cookie->setDomain($domain);
            $jar->setCookie($cookie);
        }

        $data['token']=self::getToken();

        try{
            $response = self::$client->post($url, ['json'=>$data, 'cookies'=>$jar]);
        } catch(\Exception $e){
            $handler = PrintSyndicate::getErrorReporter();
            $handler = new $handler;
            $handler->handle([
                'error'=>[
                    'http_code'=>500,
                    'message'=>$e->getMessage()
                ]
            ]);
        }



        $response = self::transformJSONAndValidateResponse($response->getBody()->getContents());
        if(isset($response['token'])){
            if($response['token'] != self::getToken()){
                self::setToken($response['token']);
            }
        }

        return $response;

    }

    public static function deleteWithToken($url, $data = []){
        self::setClient();
        $jar = new CookieJar();

        if(isset($_COOKIE['PHPSESSID'])){
            $cookie = new SetCookie();
            $cookie->setName('PHPSESSID');
            $cookie->setValue($_COOKIE['PHPSESSID']);
            $domain = PrintSyndicate::getBaseURL();
            $domain = explode('/', $domain)[0];
            $cookie->setDomain($domain);
            $jar->setCookie($cookie);
        }

        $data['token']=self::getToken();
        try{
            $response = self::$client->delete($url, ['json'=>$data, 'cookies'=>$jar]);
        } catch(\Exception $e){
            $handler = PrintSyndicate::getErrorReporter();
            $handler = new $handler;
            $handler->handle([
                'error'=>[
                    'http_code'=>500,
                    'message'=>$e->getMessage()
                ]
            ]);
        }



        $response = self::transformJSONAndValidateResponse($response->getBody()->getContents());
        if(isset($response['token'])){
            if($response['token'] != self::getToken()){
                self::setToken($response['token']);
            }
        }

        return $response;

    }


    protected static function getWithToken($url, $cacheable = true){
        self::setClient();
        $token = self::getToken();
        $url.='/'.$token;
        if($cacheable && array_key_exists($url,self::$getResultCache))
            return self::$getResultCache[$url];

        $jar = false;
        $domain = PrintSyndicate::getBaseURL();
        $domain = explode('/', $domain)[0];
        if(isset($_COOKIE['PHPSESSID'])){
            $jar = new CookieJar();
            $cookie = new SetCookie();
            $cookie->setName('PHPSESSID');
            $cookie->setValue($_COOKIE['PHPSESSID']);
            $cookie->setDomain($domain);
            $jar->setCookie($cookie);
        }
        //On the initial request after logging in we need to use the stored session id because the cookie hasn't been added yet
        if(self::$sessID != null){
            $jar = new CookieJar();
            $cookie = new SetCookie();
            $cookie->setName('PHPSESSID');
            $cookie->setValue(self::$sessID);
            $cookie->setDomain($domain);
            $jar->setCookie($cookie);
        }


        try{
            $response = self::$client->get($url, ['cookies'=>$jar]);
        } catch(\Exception $e){
            $handler = PrintSyndicate::getErrorReporter();
            $handler = new $handler;
            $handler->handle([
                'error'=>[
                    'http_code'=>500,
                    'message'=>$e->getMessage()
                ]
            ]);
        }


        $response = self::transformJSONAndValidateResponse($response->getBody()->getContents());

        //Try the method again if it fails because the token is invalid.
        if(isset($response['error'])){

            if(strpos($response['error']['message'], 'Token with Token String:') !== false){
                self::destroyCookie('token');
                return self::getWithToken($url, $cacheable);
            }
        }

        if($cacheable)
        {
            self::$getResultCache[$url] = $response;
            return self::$getResultCache[$url];
        }
        return $response;
    }

    public static function postAndStoreSession($url, $data = null){
        self::setClient();
        $jar = new CookieJar();
        try {
            $response = self::$client->post($url, ['json' => $data, 'cookies' => $jar]);
        } catch(\Exception $e){
            $handler = PrintSyndicate::getErrorReporter();
            $handler = new $handler;
            $handler->handle([
                'error'=>[
                    'http_code'=>500,
                    'message'=>$e->getMessage()
                ]
            ]);
        }
        foreach($jar->toArray() as $cookie){
            if($cookie['Name'] == 'PHPSESSID'){
                session_id($cookie['Value']);
                session_set_cookie_params(self::$cookieTimeout);
                session_start();
                self::$sessID = $cookie['Value'];
                break;
            }
        }
        return self::transformJSONAndValidateResponse($response->getBody()->getContents());
    }

    public static function transformJSONAndValidateResponse($json){
        self::$jsonStringResponse = $json;
        $response = json_decode($json, true);
        if (PrintSyndicate::isEnableErrorReporting()) {
            $errorReporterClass = PrintSyndicate::getErrorReporter();
            $errorReporter = new $errorReporterClass;
            $errorReporter->handle($response);
        }
        return $response;
    }

    public static function transformJSON($json){
        return json_decode($json, true);
    }


    public static function destroyCookie($cookieName){
        if(getenv('APP_ENV') == 'testing'){
            return;
        }
        unset($_COOKIE[$cookieName]);
        setcookie($cookieName, "", time()-360000, '/');

    }

    public static function isAuthenticated(){
        if(self::$isAuthenticated !== null)
            return self::$isAuthenticated;

        return isset($_COOKIE['isAuthenticated']) && $_COOKIE['isAuthenticated'] === "1";
    }

    protected static function validateJSONAgainstSchema($schemaPath, $endpointName = 'an un specified endpoint'){
        if(!PrintSyndicate::isEnableErrorReporting())
            return;
        //Turning off validation, it was causing more problems than it was solving

//        $errorReporter = PrintSyndicate::getErrorReporter();
//        $errorReporter = new $errorReporter;
//        $json = json_decode(self::$jsonStringResponse);
//        if(isset($json->error)){
//            $errorReporter->handle(json_decode(json_encode($json), true));
//            return;
//        }
//        $retriever = new UriRetriever();
//        $schema = $retriever->retrieve('file://' . __DIR__.'/JSONSchemas/'.$schemaPath);
//        $validator = new Validator();
//        $validator->check($json, $schema);
//
//        if(!$validator->isValid()){
//            $errors = implode(', ', array_map(function($error){
//                return $error['property'].': '.$error['message'];
//            }, $validator->getErrors()));
//            $errors = trim($errors, ', ');
//            $errorReporter->handle([
//                'error'=>[
//                    'http_code'=>620,
//                    'message'=>'There was an error parsing the schema ('.$schemaPath.') for '.$endpointName.' with the following errors: '.$errors
//                ]
//            ]);
//        }

    }

}