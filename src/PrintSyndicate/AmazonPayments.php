<?php
namespace PrintSyndicate;

class AmazonPayments extends APIResource
{
    public static function getTaxAndCountry($referenceId){
        return parent::get(PrintSyndicate::getCurrentSite().'/amazon/payments/taxAndCountryCode/'.$referenceId);
    }

    public static function checkout($referenceId, $accessToken, $methods){
        $response = parent::postWithToken(PrintSyndicate::getCurrentSite().'/amazon/payments/checkout', [
            'referenceId'=>$referenceId,
            'accessToken'=>$accessToken,
            'methods'=>$methods
        ]);
        Cart::updateItemCount();
        return $response;
    }

}