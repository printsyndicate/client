<?php
namespace PrintSyndicate;

class GiftCard extends APIResource
{
    public static function addToCart($code){
        $respnose = self::postWithToken(PrintSyndicate::getCurrentSite().'/cart/giftcard', [
            'giftcard'=>$code
        ]);

        return $respnose;
    }

    public static function getProducts(){
        return self::get(PrintSyndicate::getCurrentSite().'/giftcards');
    }
}