<?php
namespace PrintSyndicate;

class Design extends APIResource
{
    public static function getDesign($id, $additional = null){
        $route = PrintSyndicate::getCurrentSite() . '/product/' . $id;

        if($additional != null){
            $route.='/'.$additional;
        }
        $response = parent::get($route);
        self::validateJSONAgainstSchema('design.json', '/product/'.$id);
        return $response;
    }

    public static function verify($id){
        $response = parent::get(PrintSyndicate::getCurrentSite().'/product/'.$id.'/verify');
        return $response;
    }


    public static function variations($id){
        $response = parent::get(PrintSyndicate::getCurrentSite().'/product/'.$id.'/variations');
        return $response;
    }


    public static function related($id){
        $response = parent::get(PrintSyndicate::getCurrentSite().'/product/'.$id.'/related');
        return $response;
    }
}