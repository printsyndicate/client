<?php
namespace PrintSyndicate;

class Help extends APIResource
{
    public static function getShippingData(){
        return parent::get('shipping/pricing');
    }

    public static function getSizeData(){
        return parent::get(PrintSyndicate::getCurrentSite().'/inventory/sizing');
    }
}