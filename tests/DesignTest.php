<?php

require_once "APITest.php";

class DesignTest extends APITestBase
{

    public function testGetDesign(){
        try{
            $data = \PrintSyndicate\Design::getDesign(24817);
        }catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

    public function testVerify(){
        try{
            $data = \PrintSyndicate\Design::verify(24817);
        }catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

//    public function testRelated(){
//        try{
//            $data = \PrintSyndicate\Design::related(24817);
//        }catch(\Exception $e){
//            $this->fail($e->getMessage());
//        }
//    }

}