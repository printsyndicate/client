<?php

require_once "APITest.php";

class BrowseTest extends APITestBase{


    public function testHome(){
        try{
            $data = \PrintSyndicate\Browse::home();
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

    public function testGetTypes(){
        try{
            $data = \PrintSyndicate\Browse::getTypes();
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }

    }

    public function testType(){
        try{
            $data = \PrintSyndicate\Browse::type('tee', 1);
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }

    }

    public function testPill(){
        try{
            $data = \PrintSyndicate\Browse::pill('bestsellers');
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }

    }

    public function testCategory(){
        try{
            $data = \PrintSyndicate\Browse::category('clothing');
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }

    }

    public function testCollection(){
        try{
            $data = \PrintSyndicate\Browse::collection(981);
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }

    }

    public function testSearch(){
        try{
            $data = \PrintSyndicate\Browse::search('cats');
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }

    }

    public function testCollections(){
        try{
            $data = \PrintSyndicate\Browse::collections();
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }

    }
}