<?php


class APITestBase extends PHPUnit_Framework_TestCase{

    /** @var Dotenv */
    protected $env;

    public function __construct()
    {
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__.'/');
        $this->env = $dotenv;
        \PrintSyndicate\PrintSyndicate::setCurrentSite(getenv('TEST_SITE'));
        \PrintSyndicate\PrintSyndicate::setBaseURL(getenv('API_BASE_URL'));
        \PrintSyndicate\PrintSyndicate::setErrorReporter(\PrintSyndicate\Errors\TestingErrorReporter::class);
        parent::__construct();
    }

    public function testPass(){
        $this->assertTrue(true);
    }
}