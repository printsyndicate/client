<?php

require_once "APITest.php";

class CartTest extends APITestBase{

    protected function setUp()
    {
        $_COOKIE['token'] = getenv('TOKEN');
        parent::setUp();
    }

    public function testAdd(){
        try{
            $data = \PrintSyndicate\Cart::add('24817-6010-heathered_gray_nl-md');
        }catch(\Exception $e){
            $this->fail($e->getMessage());
        }
        $this->env->setEnvironmentVariable('cartId', $data['id']);
        return $data['items'][0]['id'];
    }

    public function testAddGiftCard(){
        try{
            $data = \PrintSyndicate\GiftCard::addToCart(getenv('GIFT_CARD_CODE'));
        }catch(\Exception $e){
            $this->fail($e->getMessage());
        }
        $this->env->setEnvironmentVariable('giftCardId', $data['giftCards'][0]['id']);
    }

    /**
     * @depends testAddGiftCard
     */
    public function testRemoveGiftCard(){
        try{
            $data = \PrintSyndicate\Cart::removeGiftCard(getenv('giftCardId'));
        }catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

    public function testAddCoupon(){
       try{
           $data = \PrintSyndicate\Coupon::addToCart('welcome10');
       } catch(\Exception $e){
           $this->fail($e->getMessage());
       }
    }

    /**
     * @depends testAddCoupon
     */
    public function testRemoveCoupon(){
        try{
            $data = \PrintSyndicate\Cart::removeCoupon('welcome10');
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

    public function testGetFlyoutData(){
        try{
            $data = \PrintSyndicate\Cart::getFlyoutData();
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }
    public function testGetCart(){
        try{
            $data = \PrintSyndicate\Cart::getCart();
        }catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

    /**
     * @depends testAdd
     */
    public function testUpdateItem($id){
        try{
            $data = \PrintSyndicate\Cart::updateItem($id, null, 15);
        }catch(\Exception $e){
            $this->fail($e->getMessage());
        }
        $this->assertEquals(15, $data['items'][0]['quantity']);
        return $id;
    }


    /**
     * @depends testUpdateItem
     */
    public function testRemoveItem($id){
        try{
            $data = \PrintSyndicate\Cart::removeItem($id);
        }catch(\Exception $e){
            $this->fail($e->getMessage());
        }

        \PrintSyndicate\Cart::add('24817-6010-heathered_gray_nl-md');
    }

    public function testAddShippingAddress(){
        try{
            $data = \PrintSyndicate\Cart::addShippingAddress(getenv('cartId'), '123 Fake St', null, 'Columubs', 'OH', 'US', '43215', 'test@testington.com', '1234567890', 'Billy', 'Madison', false);
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

    public function testGetShippingMethods(){
        try{
            $data = \PrintSyndicate\Cart::getShippingMethods();
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

    public function testSetShippingMethods(){
        try{
            $data = \PrintSyndicate\Cart::setShippingMethods(['8'=>'6']);
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }
    }

    public function testCheckout(){
        $source = $this->getStripeSource();
        try{
            $data = \PrintSyndicate\Cart::checkout($source, 'USD', 'desktop', 'magic', '123 Fake St', null, '43215', 'Columbus', 'OH', 'US', 'Bobby', 'Goodman', 'fake@fake.com', '123456689');
        } catch(\Exception $e){
            $this->fail($e->getMessage());
        }
        $this->assertTrue($data['purchased']);
    }

    public function getStripeSource(){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.stripe.com/v1/tokens",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "key=pk_test_vtdS6M9X0I0Wj6hHmaemdhcI&payment_user_agent=stripe.js%2F8a1a466&card%5Bnumber%5D=4242424242424242&card%5Bcvc%5D=424&card%5Bexp_month%5D=11&card%5Bexp_year%5D=2016",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer sk_test_wQaZYEDHjK0uebZzzucyVhWi",
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: fbc4c00e-f343-c19b-43eb-0bd43163adcb"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new \Exception('Stripe failed to provide a token! Cannot test checkout.');
        } else {
            $data = json_decode($response, true);
            return $data['id'];
        }
    }

    public function testLookupOrder(){

    }

    public function testReceipt(){

    }

    public function testUpdateShipmentItem(){

    }
    public function testUpdateShippingAddress(){

    }

}